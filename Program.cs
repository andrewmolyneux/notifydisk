﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Threading;
using System.Windows.Forms;

namespace NotifyDisk
{
    static class Program
    {
        // Configuration :)
        private static string driveLetter = "c";
        private static TimeSpan updateInterval = TimeSpan.FromMilliseconds(500);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        extern static bool DestroyIcon(IntPtr handle);

        static byte[] HashBitmap(Bitmap bmp)
        {
            using (var hash = SHA256.Create())
            {
                using (var stream = new MemoryStream())
                {
                    bmp.Save(stream, ImageFormat.Png);
                    stream.Seek(0, SeekOrigin.Begin);
                    return hash.ComputeHash(stream);
                }
            }
        }

        static Bitmap GenerateIconBitmap(int percentUsed)
        {
            Color brushColor;
            int ymax = 100;
            if (percentUsed >= 90)
            {
                brushColor = Color.Red;
            }
            else if (percentUsed >= 80)
            {
                brushColor = Color.Yellow;
            }
            else
            {
                brushColor = Color.Green;
            }
            const int iconSize = 16;
            var bmp = new Bitmap(iconSize, iconSize);
            using (var graphics = Graphics.FromImage(bmp))
            {
                using (var brush = new SolidBrush(brushColor))
                {
                    graphics.FillRectangle(SystemBrushes.ControlDark, 0, 0, iconSize, iconSize);
                    // Make sure at least one pixel is visible, even if disk usage is very low
                    int fillHeight = Math.Max((iconSize * percentUsed) / ymax, 1);
                    graphics.FillRectangle(brush, 0, iconSize - fillHeight, iconSize, iconSize);
                    graphics.DrawRectangle(SystemPens.WindowFrame, 0, 0, iconSize, iconSize);
                }
            }
            return bmp;
        }

        static Icon GenerateIcon(int percentUsed)
        {
            using (var bmp = GenerateIconBitmap(percentUsed))
            {
                return Icon.FromHandle(bmp.GetHicon());
            }
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var icons = new List<Icon>();
            var percentageIcons = new Icon[101];
            var lastHash = new byte[32];
            for (int i = 0; i < 101; i++)
            {
                using (var bmp = GenerateIconBitmap(i))
                {
                    var thisHash = HashBitmap(bmp);
                    if (!thisHash.SequenceEqual(lastHash))
                    {
                        lastHash = thisHash;
                        icons.Add(Icon.FromHandle(bmp.GetHicon()));
                    }
                    percentageIcons[i] = icons[icons.Count - 1];
                }
            }
            using (var container = new Container())
            {
                using (var worker = new BackgroundWorker())
                {
                    var quitMenuItem = new MenuItem();
                    var contextMenu = new ContextMenu();
                    var notify = new NotifyIcon();
                    contextMenu.MenuItems.AddRange(new MenuItem[] { quitMenuItem });
                    quitMenuItem.Index = 0;
                    quitMenuItem.Text = "E&xit";
                    quitMenuItem.Click += new EventHandler((object sender, EventArgs e) =>
                    {
                        worker.CancelAsync();
                    });
                    notify.ContextMenu = contextMenu;
                    notify.Visible = true;
                    worker.WorkerReportsProgress = true;
                    worker.WorkerSupportsCancellation = true;
                    worker.DoWork += new DoWorkEventHandler((object sender, DoWorkEventArgs e) =>
                    {
                        while (!worker.CancellationPending)
                        {
                            worker.ReportProgress(0);
                            Thread.Sleep(updateInterval);
                        }
                    });
                    worker.ProgressChanged += new ProgressChangedEventHandler((object sender, ProgressChangedEventArgs e) =>
                    {
                        var drive = new DriveInfo(driveLetter);
                        double totalGigabytes = (double)drive.TotalSize / (1024.0 * 1024.0 * 1024.0);
                        double freeGigabytes = (double)drive.TotalFreeSpace / (1024.0 * 1024.0 * 1024.0);
                        int percentFree = (int)(drive.TotalFreeSpace * (long)100 / drive.TotalSize);
                        notify.Text = String.Format("Drive {0}:\n{1:0.00} GB ({2}%) free\n{3:0.00} GB total", driveLetter.ToUpperInvariant(), freeGigabytes, percentFree, totalGigabytes);
                        notify.Icon = percentageIcons[100 - percentFree];
                    });
                    worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler((object sender, RunWorkerCompletedEventArgs e) =>
                    {
                        Application.Exit();
                    });
                    worker.RunWorkerAsync();
                    Application.Run();
                }
            }
        }
    }
}
