# NotifyDisk
### A tiny to show free disk space in the taskbar notification area, written in C#

There's no configuration UI, so if you want it to show disk usage for
anything other than drive C, you'll need to change the "driveLetter"
property in the source code. If you don't agree with the update interval
(0.5 second), you can change that too.

In fact, the only UI is a tooltip showing more detailed information
about disk free space vs. total size, and a right-click menu with an
"Exit" option.

Once you've got the thing running, it probably makes sense to customise
notification icons and set it to "Show icon and notifications" so it
stays visible.

__NOTE__: Windows is paranoid about the possibility that apps might try
to impersonate other apps in order to keep their notification icons
visible. As a result, if you run this app, tell Windows to keep it
visible, then run it again from a different path (e.g. if you run a
debug build then a release build later, or copy the EXE to your Startup
folder) Windows will probably start hiding the icon, until you log off
and on again. This perplexed me briefly :D To avoid having to perform
this little dance, I recommend copying the EXE to its final destination
before running it and setting its visibility.
